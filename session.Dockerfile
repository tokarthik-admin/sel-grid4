FROM selenium4-base:1.0
MAINTAINER Ragavan Ambighananthan <rambighananthan@expedia.com>
EXPOSE 5556
#========================================
# Session Mapper Start up
#========================================
ENTRYPOINT java -Djava.net.preferIPv4Stack=true -jar /opt/selenium/selenium-server-4.0.0-alpha-4.jar sessions