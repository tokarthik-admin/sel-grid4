FROM selenium4-base:1.0
MAINTAINER Ragavan Ambighananthan <rambighananthan@expedia.com>

#========================================
# Session Mapper Start up
#========================================
ENTRYPOINT java -Djava.net.preferIPv4Stack=true -jar /opt/selenium/selenium-server-4.0.0-alpha-4.jar router --sessions http://localhost:5556 --distributor http://localhost:5553
