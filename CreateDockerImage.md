# Selenium 4 Grid

## Setup

## Session Mapper - Port 5556
```java -Djava.net.preferIPv4Stack=true -jar selenium-server-4.0.0-alpha-4.jar sessions```

## Distributer - port 5553

```java -Djava.net.preferIPv4Stack=true -jar selenium-server-4.0.0-alpha-4.jar distributor --sessions http://localhost:5556```

## Router - port 4444
```java -Djava.net.preferIPv4Stack=true -jar selenium-server-4.0.0-alpha-4.jar router --sessions http://localhost:5556 --distributor http://localhost:5553```

## Node Port 5555
```java -jar selenium-server-4.0.0-alpha-4.jar node --detect-drivers --publish-events tcp://localhost:4442 --subscribe-events tcp://localhost:4443```



# Compile Souce
- install jdk11 or greater
- brew install python
- install xcode and run
- sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer
- sudo xcodebuild -license
- bazel build java/...
- bazel build grid

## References
- https://github.com/SeleniumHQ/selenium/wiki/Selenium-Grid-4
- https://codoid.com/selenium-grid-4-distributed-execution/

How to run the Docker locally

``` docker run --name sessions -it -p 5556:5556 -v /var/run/docker.sock:/var/run/docker.sock selenium4-base:1.0 java -Djava.net.preferIPv4Stack=true -jar /opt/selenium/selenium-server-4.0.0-alpha-4.jar sessions  ```


``` docker run --name router  -it -p 4444:4444 -v /var/run/docker.sock:/var/run/docker.sock selenium4-base:1.0  java -Djava.net.preferIPv4Stack=true -jar /opt/selenium/selenium-server-4.0.0-alpha-4.jar router --sessions http://localhost:5556 --distributor http://localhost:5553```


``` docker run --name distributor -it -p 5553:5553 -v /var/run/docker.sock:/var/run/docker.sock selenium4-base:1.0  java -Djava.net.preferIPv4Stack=true -jar /opt/selenium/selenium-server-4.0.0-alpha-4.jar distributor --sessions http://localhost:5556 ```



``` docker run -it --name node -p 4443:4443 -p 4442:4442 -v /var/run/docker.sock:/var/run/docker.sock selenium4-base:1.0  java -Djava.net.preferIPv4Stack=true -jar /opt/selenium/selenium-server-4.0.0-alpha-4.jar node --detect-drivers --publish-events tcp://localhost:4442 --subscribe-events tcp://localhost:4443 ```

