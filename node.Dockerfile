FROM selenium4-base:1.0
MAINTAINER Ragavan Ambighananthan <rambighananthan@expedia.com>
EXPOSE 4442
EXPOSE 4443
CMD docker pull selenium/node-chrome:latest
#========================================
# Session Mapper Start up
#========================================
COPY selenium-server-4.0.0-alpha-4.jar /opt/selenium/
ENTRYPOINT java -jar /opt/selenium/selenium-server-4.0.0-alpha-4.jar node -D selenium/node-chrome:latest '{"browserName": "chrome"}' --publish-events tcp://localhost:4442 --subscribe-events tcp://localhost:4443
